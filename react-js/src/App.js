import React, { useEffect } from 'react';
import { AiFillEdit } from 'react-icons/ai';
import { AiFillDelete } from 'react-icons/ai';
import { AiOutlineCheckCircle } from 'react-icons/ai';

import "./App.css";

function Todo({ todo, index, completeTodo, removeTodo, updateTodo }) {
  return (
    <div
      className="todo"
      style={{ textDecoration: todo.isCompleted ? "line-through" : "" }}
    >
      {todo.text}
      <div>
        <button className="btn-complete" onClick={() => completeTodo(index)}><AiOutlineCheckCircle /></button>
        <button className="btn-update" onClick={() => updateTodo(index)}><AiFillEdit/></button>
        <button className="btn-remove" onClick={() => removeTodo(index)}><AiFillDelete/></button>
        
      </div>
    </div>
  );
}

function TodoForm({ addTodo }) {
  const [value, setValue] = React.useState("");

  const handleSubmit = e => {
    e.preventDefault();
    if (!value) return;
    addTodo(value);
    setValue("");
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        className="input"
        value={value}
        onChange={e => setValue(e.target.value)}
      />
    </form>
  );
}

// Get localstorage
const getLocalStorage = () => {
  let todo = localStorage.getItem('todos');
  if (todo) {
    return (todo = JSON.parse(localStorage.getItem('todos')));
  } else {
    return [];
  }
};

function App() {
  const [todos, setTodos] = React.useState(getLocalStorage);

  // set localstorage
  useEffect(() => {
    localStorage.setItem('todos', JSON.stringify(todos));
  }, [todos]);


  const addTodo = text => {
    const newTodos = [...todos, { text }];
    setTodos(newTodos);
  };

  const completeTodo = index => {
    const newTodos = [...todos];
    newTodos[index].isCompleted = true;
    setTodos(newTodos);
  };

  const removeTodo = index => {
    const newTodos = [...todos];
    newTodos.splice(index, 1);
    setTodos(newTodos);
  };
  const updateTodo = (index) => {
    const newTodo = [...todos];

    let item = newTodo[index];
    
    let newItem = prompt(`Ingin Update Todo List?`, item.text);
  
    newTodo.splice(index, 1, {text: newItem});
    setTodos(newTodo);
  };

  useEffect(() => {
    const storedTodos = JSON.parse(localStorage.getItem('todos')) || [];
    setTodos(storedTodos);
  }, []);



  return (
    <div className="app">
      <div className="todo-list">
        <div className="title">
          <h1>Todo List</h1>
        {todos.map((todo, index) => (
          <Todo
            key={index}
            index={index}
            todo={todo}
            completeTodo={completeTodo}
            removeTodo={removeTodo}
            updateTodo={updateTodo}
          />
        ))}
        <TodoForm addTodo={addTodo} />
      </div>
    </div>
    </div>
  );
}

export default App;
